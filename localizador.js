// Variable del mapa
var x = $("#map");

function getLocation() {
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition, showError);
  } else {
      x.html("Geolocation is not supported by this browser.");
  }
}
function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}
function getCoords(marker){
  $("#Inlat").val(marker.getPosition().lat());
  $("#Inlon").val(marker.getPosition().lng());
}
function showPosition(position) {
  var myLatlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
  var myOptions = {
    zoom: 16,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  }
  var map = new google.maps.Map(document.getElementById("map"), myOptions);
  marker = new google.maps.Marker({
    position: myLatlng,
    draggable: true,
    title:"Aquí Es!!"
  });
  google.maps.event.addListener(marker, "dragend", function() {
    getCoords(marker);
  });
  marker.setMap(map);
  getCoords(marker);
}
function cargar() {
  var myLatlng = new google.maps.LatLng($("#Inlat").val(),$("#Inlon").val());
  var myOptions = {
    zoom: 16,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  }
  var map = new google.maps.Map(document.getElementById("map"), myOptions);
  marker = new google.maps.Marker({
    position: myLatlng,
    draggable: true,
    title:"Aquí Es!!"
  });
  google.maps.event.addListener(marker, "dragend", function() {
    getCoords(marker);
  });
  marker.setMap(map);
  getCoords(marker);
}
